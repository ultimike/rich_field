<?php

namespace Drupal\rich_field\Plugin\Field;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Methods common to all "Rich" field plugins (items, widgets, and formatters).
 */
trait RichFieldTrait {

  /**
   * The subfield instances.
   *
   * @var \Drupal\Core\Field\FieldItemInterface[]|\Drupal\Core\Field\WidgetInterface[]|\Drupal\Core\Field\FormatterInterface[]
   */
  protected $subfields = [];

  /**
   * Provides the field plugin manager service appropriate to this plugin type.
   *
   * @return \Drupal\Core\Field\FieldTypePluginManagerInterface|\Drupal\Core\Field\WidgetPluginManager|\Drupal\Core\Field\FormatterPluginManager
   *   The field plugin manager service appropriate to this plugin type.
   */
  abstract protected static function fieldPluginManager();

  /**
   * Provides the list of subfield types, keyed by user-specified machine name.
   *
   * @todo Expose this setting via field UI.
   *
   * @return array
   *   The list of subfield types, keyed by user-specified machine name.
   */
  abstract protected static function getSubfieldTypes();

  /**
   * Gets the field plugin type for the subfield with the given machine name.
   *
   * @param string $name
   *   The name of the subfield for which to return the field type.
   *
   * @return string
   *   The subfield type.
   */
  protected static function getSubfieldType($name) {
    static $types;
    if (!isset($types)) {
      $types = static::getSubfieldTypes();
    }
    return $types[$name];
  }

  /**
   * Provides the list of subfield instances.
   *
   * @return array
   *   The list of subfield instances.
   */
  public function getSubfields() {
    return $this->subfields;
  }

  /**
   * Returns the subfield instance with the given machine name.
   *
   * @return array
   *   The subfield instance with the given machine name.
   */
  public function getSubfield($name) {
    return $this->subfields[$name];
  }

  /**
   * Provides the plugin classes for each subfield item type.
   *
   * @return array
   *   The list of subfield item classes, keyed by subfield name.
   */
  protected static function getSubfieldClasses() {
    static $classes;
    return $classes ?: $classes = array_map(function ($type) {
      return static::fieldPluginManager()
        ->getDefinition($type)['class'];
    }, static::getSubfieldTypes());
  }

  /**
   * Provides the plugin class for the subfield with the given name.
   *
   * @param string $name
   *   The name of the subfield for which to return the plugin class.
   *
   * @return string
   *   The subfield's plugin class.
   */
  protected static function getSubfieldClass($name) {
    static $classes;
    if (!isset($classes)) {
      $classes = static::getSubfieldTypes();
    }
    return $classes[$name];
  }

  /**
   * Determines the proper subfield name based on the field name and subfield.
   *
   * @param string $name
   *   The key to alter.
   * @param string $subname
   *   The name of the subfield to use for altering the key.
   *
   * @return string
   *   The altered key.
   */
  protected static function getSubfieldName($name, $subname) {
    return $name . '__' . $subname;
  }

  /**
   * Returns the prefix to use in altering subfield keys.
   *
   * Since we can't have constants in traits.
   *
   * @todo Change this when the module name changes.
   *
   * @return string
   *   The prefix to use in altering subfield keys.
   */
  final protected static function getPrefix() {
    return 'rich_field';
  }

  /**
   * Adds a standard prefix to the given string based on the specified subfield.
   */
  protected static function alterKey($key, $subfield) {
    return static::getPrefix() . '__' . $subfield . '__' . $key;
  }

  /**
   * Adds a standard prefix to the given string based on the specified subfield.
   *
   * @param string $key
   *   The key to alter by reference.
   * @param string $subfield
   *   The name of the subfield to use for altering the key.
   */
  protected static function alterKeyByReference(&$key, $subfield) {
    $key = static::alterKey($key, $subfield);
  }

  /**
   * Adds a standard prefix to the array keys based on the specified subfield.
   *
   * @param array $associative_array
   *   The array with keys to alter.
   * @param string $subfield
   *   The name of the subfield to use in altering the array keys.
   *
   * @return array
   *   The given array with keys altered.
   */
  protected static function alterKeys(array $associative_array, $subfield) {
    foreach ($associative_array as $key => &$value) {
      $altered_array[static::alterKey($key, $subfield)] = $value;
    }
    return $altered_array ?? [];
  }

  /**
   * Adds a standard prefix to the array keys based on the specified subfield.
   *
   * @param array $associative_array
   *   The array with keys to alter by reference.
   * @param string $subfield
   *   The name of the subfield to use in altering the array keys.
   */
  protected static function alterKeysByReference(array &$associative_array, $subfield) {
    foreach (array_keys($associative_array) as $key) {
      $altered_key = static::alterKey($key, $subfield);
      $associative_array[$altered_key] = &$associative_array[$key];
      unset($associative_array[$key]);
    }
  }

  /**
   * Unalters the given string by removing its subfield prefix.
   *
   * @param string $key
   *   The key to normalize.
   * @param string $subfield
   *   The name of the subfield to use in normalizing the key.
   *
   * @return string
   *   The normalized key, or FALSE if the specified subfield's prefix was not
   *   found.
   */
  protected static function normalizeKey($key, $subfield) {
    $prefix = static::alterKey('', $subfield);
    return strpos($key, $prefix) === 0 ? substr($key, strlen($prefix)) : FALSE;
  }

  /**
   * Unalters the keys of the given array by removing the subfield prefix.
   *
   * Removes keys that belong to other subfields but preserves keys that don't
   * belong to any subfield.
   *
   * @param array $associative_array
   *   The array with keys to normalize.
   * @param string $subfield
   *   The name of the subfield for which to return the "normalized" array.
   *
   * @return array
   *   The given array with subfield keys normalized, non-subfield items
   *   preserved, and items that belong to a different subfield removed.
   */
  protected static function normalizeKeys(array $associative_array, $subfield) {
    foreach ($associative_array as $key => &$value) {

      // Preserve "parent field" items that don't belong to any subfield.
      if (strpos($key, static::getPrefix()) !== 0) {
        $normalized_array[$key] = $value;
      }
      else {
        $normalized_key = static::normalizeKey($key, $subfield);
        if ($normalized_key !== FALSE) {
          $normalized_array[$normalized_key] = $value;
        }
      }

    }
    return $normalized_array ?? [];
  }

  /**
   * Pulls the given subfield from each rich field item into a new item list.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The rich field items from which to isolate subfields.
   * @param string $subfield_name
   *   The name of the subfield to pull from the rich field items.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   A new field item list made of the subfields from the "rich" item list.
   */
  protected static function normalizeItems(FieldItemListInterface $items, $subfield_name) {
    /** @var \Drupal\rich_field\Plugin\Field\FieldType\RichItem $item */
    foreach ($items as $delta => $item) {
      $subfield = $item
        ->getSubfield($subfield_name);
      // Initialize a new item list on first pass.
      if (!isset($normalized_items)) {
        /** @var \Drupal\Core\Field\FieldItemListInterface $list_class */
        $list_class = $subfield
          ->getFieldDefinition()
          ->getClass();
        /** @var \Drupal\Core\Field\FieldItemListInterface $normalized_items */
        $normalized_items = $list_class::createInstance($subfield
          ->getFieldDefinition(), $subfield
          ->getName(), $items
          ->getParent());
      }
      $normalized_items[$delta] = $subfield
        ->getValue();
    }
    return $normalized_items ?? NULL;
  }

  /**
   * Calls the specified method on all subfields.
   *
   * @param string $method
   *   The method to call on each subfield.
   * @param array|null $args
   *   Additional arguments (if any) to pass into the method call.
   *
   * @return \Drupal\Core\Field\FieldItemInterface
   *   Returns itself to allow for chaining.
   */
  protected function call($method, ...$args) {
    foreach ($this
      ->getSubfields() as $subfield) {
      $subfield
        ->$method(...$args);
    }
    return parent::$method(...$args);
  }

  /**
   * Calls the specified method on all subfields and combines their results.
   *
   * @param string $method
   *   The method to call on each subfield.
   * @param array|null $args
   *   Additional arguments (if any) to pass into the method call.
   *
   * @return array
   *   The subfield results with keys altered.
   */
  protected static function union($method, ...$args) {
    $union = [];
    foreach (static::getSubfieldClasses() as $name => $class) {
      $union += static::alterKeys($class::$method(...$args), $name);
    }
    return parent::$method(...$args) + $union;
  }

  /**
   * Calls the specified method on all subfields and combines their results.
   *
   * @param string $method
   *   The method to call on each subfield.
   * @param array|null $args
   *   Additional arguments (if any) to pass into the method call.
   *
   * @return array
   *   The subfield results with keys altered.
   */
  protected function instanceUnion($method, ...$args) {
    $union = [];
    foreach ($this
      ->getSubfields() as $name => $subfield) {
      $union += static::alterKeys($subfield
        ->$method(...$args), $name);
    }
    return parent::$method(...$args) + $union;
  }

  /**
   * Calls the specified method on all subfields and combines their results.
   *
   * @param string $method
   *   The method to call on each subfield.
   * @param \Drupal\Core\Field\FieldDefinitionInterface|\Drupal\Core\Field\FieldStorageDefinitionInterface $field_definition
   *   The field definition or field storage definition to pass into each method
   *   call.
   * @param array|null $args
   *   Additional arguments (if any) to pass into the method call.
   *
   * @return array
   *   The subfield results with keys altered.
   */
  protected static function unionWithFieldDefinition($method, $field_definition, ...$args) {
    $union = [];
    foreach (static::getSubfieldClasses() as $name => $class) {
      $subfield_definition = static::getSubfieldDefinition($field_definition, $name);
      $results = $class::$method($subfield_definition, ...$args);
      $union += static::alterKeys($results, $name);
    }
    return parent::$method($field_definition, ...$args) + $union;
  }

  /**
   * Calls the specified method on all subfields and combines their results.
   *
   * @param string $method
   *   The method to call on each subfield.
   * @param array $settings
   *   The settings array to pass into each method call.
   * @param array|null $args
   *   Additional arguments (if any) to pass into the method call.
   */
  protected static function unionWithSettings($method, array $settings, ...$args) {
    $settings += parent::$method($settings, ...$args);
    foreach (static::getSubfieldClasses() as $name => $class) {
      $normalized_settings = static::normalizeKeys($settings, $name);
      $subsettings = $class::$method($normalized_settings, ...$args);
      $settings += static::alterKeys($subsettings, $name);
    }
    return $settings;
  }

  /**
   * Calls the specified method on all subfields and combines their results.
   *
   * @param string $method
   *   The method to call on each subfield.
   * @param array $parents
   *   The form's '#parents' value.
   * @param array|null $args
   *   Additional arguments (if any) to pass into the method call.
   *
   * @return array
   *   The combined form array.
   *
   * @todo Subform callbacks recieve a form state with the parent's field
   * definition. This breaks e.g. entity reference settings form validation. To
   * fix, wrap the form callbacks in our own callback to normalize the form
   * state before sending it to the original callback.
   */
  protected function unionWithForm($method, array $parents, $form, $form_state, ...$args) {
    $return = parent::$method($form, $form_state, ...$args) ?: [];

    foreach ($this
      ->getSubfields() as $name => $subfield) {
      // @todo Won't prefixing the form element keys break any subfield logic
      // that tries to access the form data? So do we need to add validate and
      // submit callbacks to normalize the data in each subform?
      $field_definition = $this->fieldDefinition ?? $this
        ->getFieldDefinition();
      $subform_state = clone $form_state;
      $field_config_edit_form = clone $subform_state
        ->getFormObject();
      $subform_state
        ->setFormObject($field_config_edit_form
          ->setEntity(static::getSubfieldDefinition($field_definition, $name)
            ->getConfig('page')));
      $subform = static::normalizeKeys($subfield
        ->$method($form, $subform_state, ...$args), $name);
      if ($subform) {
        $return[$name] = [
          '#type' => 'fieldset',
          '#title' => $subfield
            ->getPluginDefinition()['label'],
          '#parents' => $parents,
        ] + $subform;
      }
    }
    return $return;
  }

  /**
   * Returns the field definition for a subfield.
   *
   * Although we have two different types of field definitions (storage and
   * regular), returning instances of BaseFieldDefinition seems to work for both
   * (it implements both interfaces).
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface|\Drupal\Core\Field\FieldStorageDefinitionInterface $field_definition
   *   The main field's field definition.
   * @param string $subfield
   *   The name of the subfield for which to return a field definition.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   The subfield's field definition.
   */
  protected static function getSubfieldDefinition($field_definition, $subfield) {
    $type = static::getSubfieldType($subfield);
    $primary_label = $field_definition
      ->getLabel();
    $secondary_label = strtolower(static::fieldPluginManager()
      ->getDefinition($type)['label']);
    return BaseFieldDefinition::create($type)
      ->setName(static::getSubfieldName($field_definition
        ->getName(), $subfield))
      ->setLabel("$primary_label ($secondary_label)")
      ->setSettings(static::normalizeKeys($field_definition
        ->getSettings(), $subfield))
      ->setTargetEntityTypeId($field_definition
        ->getTargetEntityTypeId());
  }

}
