<?php

namespace Drupal\Tests\rich_field\Kernel;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\rich_field\Plugin\Field\FieldType\RichItem;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Defines a class for testing basic functionality.
 *
 * @group rich_Field
 */
class RichFieldTest extends KernelTestBase {

  /**
   * @todo remove this hack when the schema exists.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'rich_field',
    'telephone',
    'entity_test',
    'system',
    'field',
    'text',
    'user',
    'link',
    'filter',
    'filter_test',
    'taxonomy',
  ];

  /**
   * Test vocabulary.
   *
   * @var \Drupal\taxonomy\VocabularyInterface
   */
  protected $vocabulary;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installConfig(['filter', 'filter_test']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('taxonomy_term');
    $this->vocabulary = Vocabulary::create([
      'name' => 'Categories',
      'vid' => 'categories',
    ]);
    $this->vocabulary->save();
    $this->installEntitySchema('entity_test');
    $storage = FieldStorageConfig::create([
      'field_name' => 'applicant',
      'entity_type' => 'entity_test',
      'type' => 'rich_field',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ]);
    $storage->save();
    $field = FieldConfig::create([
      'field_name' => 'applicant',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'label' => 'Name',
    ]);
    $field->save();
  }

  /**
   * Tests rich field.
   */
  public function testRichField() {
    $entity = $this->assertEntityCreationStorageAndRetrieval();
    $item = $this->assertCreationOfAdditionalFieldItem($entity);
    $this->assertUpdatingFieldValuesViaProxies($item);
    $this->assertUnsettingFieldValuesViaProxies($item);
    $this->fail('No config schema 🤕');
  }

  /**
   * Tests creation storage and retrieval.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   Created entity.
   */
  protected function assertEntityCreationStorageAndRetrieval() {
    // Note we don't save this so we can test the preSave methods.
    $accepted = Term::create([
      'vid' => $this->vocabulary->id(),
      'name' => 'Accepted',
    ]);
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = EntityTest::create([
      'name' => 'entity',
      'applicant' => [
        'foo' => 'Jerry',
        'bar' => '12345678',
        'bat' => [
          'uri' => 'http://example.com/jerry.johnson',
          'title' => 'My resume',
        ],
        'whiz' => [
          'value' => '<p>My bio<strong>is amazing!</strong></p><div>but divs are not allowed</div>',
          'format' => 'filtered_html',
        ],
        'baz' => $accepted,
      ],
    ]);
    $entity->save();
    $storage = $this->container->get('entity_type.manager')
      ->getStorage('entity_test');
    $storage->resetCache();
    $entity = $storage->load($entity->id());
    $this->assertEquals('Jerry', $entity->applicant->foo->value);
    $this->assertEquals('12345678', $entity->applicant->bar->value);
    $this->assertEquals('http://example.com/jerry.johnson', $entity->applicant->bat->uri);
    $this->assertEquals('My resume', $entity->applicant->bat->title);
    $this->assertEquals('<p>My bio<strong>is amazing!</strong></p><div>but divs are not allowed</div>', $entity->applicant->whiz->value);
    $this->assertEquals('filtered_html', $entity->applicant->whiz->format);
    $this->assertEquals('<p>My bio<strong>is amazing!</strong></p>but divs are not allowed', $entity->applicant->whiz->processed);
    $this->assertEquals($accepted->id(), $entity->applicant->baz->entity->id());

    $this->assertEquals('Accepted', $entity->applicant->baz->entity->label());
    return $entity;
  }

  /**
   * Asserts creation and addition of a new item.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to add items to.
   *
   * @return \Drupal\rich_field\Plugin\Field\FieldType\RichItem
   *   Rich item created.
   */
  protected function assertCreationOfAdditionalFieldItem(ContentEntityInterface $entity) {
    $entity->applicant[] = [
      'foo' => 'Sally',
      'bar' => '12345678',
      'bat' => [
        'uri' => 'http://example.com/sally.samson',
        'title' => 'My resume',
      ],
      'whiz' => [
        'value' => '<p>Hi there</p>',
        'format' => 'filtered_html',
      ],
    ];
    $item = $entity->get('applicant')->get(1);
    $this->assertEquals('Sally', $item->foo->value);
    $this->assertEquals('12345678', $item->bar->value);
    $this->assertEquals('http://example.com/sally.samson', $item->bat->uri);
    $this->assertEquals('My resume', $item->bat->title);
    $this->assertEquals('<p>Hi there</p>', $item->whiz->value);
    $this->assertEquals('filtered_html', $item->whiz->format);
    $this->assertEquals('<p>Hi there</p>', $item->whiz->processed);
    return $item;
  }

  /**
   * Tests updating item properties using the proxy properties.
   *
   * @param \Drupal\rich_field\Plugin\Field\FieldType\RichItem $item
   *   Item to update.
   */
  protected function assertUpdatingFieldValuesViaProxies(RichItem $item) {
    $item->foo = 'Sally-Ann';
    $item->bar->value = '12345678';
    $this->assertEquals('Sally-Ann', $item->foo->value);
    $this->assertEquals('12345678', $item->bar->value);
    $this->assertEquals([
      'rich_field__foo__value' => 'Sally-Ann',
      'rich_field__bar__value' => '12345678',
      'rich_field__bat__uri' => 'http://example.com/sally.samson',
      'rich_field__bat__title' => 'My resume',
      'rich_field__bat__options' => [],
      'rich_field__whiz__value' => '<p>Hi there</p>',
      'rich_field__whiz__format' => 'filtered_html',
    ], $item->getValue());
  }

  /**
   * Test unsetting works.
   *
   * @param \Drupal\rich_field\Plugin\Field\FieldType\RichItem $item
   *   Rich field item to update.
   */
  protected function assertUnsettingFieldValuesViaProxies(RichItem $item) {
    $item->foo = NULL;
    $item->bar->value = NULL;
    $values = [
      'rich_field__bat__uri' => 'http://example.com/sally.samson',
      'rich_field__bat__title' => 'My resume',
      'rich_field__whiz__value' => '<p>Hi there</p>',
      'rich_field__whiz__format' => 'filtered_html',
    ];
    $this->assertEquals($values, array_filter($item->getValue()));

    // Link field has multiple properties, but also a main property name.
    // Setting the field proxy to null should set the uri to null, but not the
    // title (this is how LinkItem works).
    $item->bat = NULL;
    $this->assertEquals([
      'rich_field__bat__title' => 'My resume',
      'rich_field__whiz__value' => '<p>Hi there</p>',
      'rich_field__whiz__format' => 'filtered_html',
    ], array_filter($item->getValue()));
  }

  /**
   * Tests isEmpty functionality.
   */
  public function testIsEmpty() {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = EntityTest::create([
      'name' => 'entity',
    ]);
    $this->assertTrue($entity->get('applicant')->isEmpty());
    $entity = EntityTest::create([
      'name' => 'entity',
      'applicant' => [
        'foo' => 'Jerry',
        'bat' => [
          'uri' => 'http://example.com/jerry.johnson',
        ],
      ],
    ]);
    $this->assertFalse($entity->get('applicant')->isEmpty());
    $this->fail('No config schema 🤕');
  }

}
